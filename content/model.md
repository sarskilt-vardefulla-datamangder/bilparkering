# Datamodell

Datamodellen är tabulär, det innebär att varje rad motsvarar exakt en parkering och varje kolumn motsvarar en egenskap för det parkeringen. 23 attribut är definierade, där de första 6 är obligatoriska. Denna modell tar ett enklare och mer översiktligt grepp än [tidigare projekt](https://www.dataportal.se/en/specifications/CAAPWaterQuality/parking) som kan byggas vidare och kopplas ihop med datamängder som följer denna dataspecifikation.

<div class="note" title="1">
Vi väljer att använda beskrivande men korta kolumnnamn som uttrycks med gemener, utan mellanslag (understreck för att separera ord) och på engelska.

Genom engelska attributnamn blir modellen enklare att hantera i programvaror och tjänster utvecklade utanför Sverige. 


</div>

<div class="note" title="2">


<div class="ms_datatable">

| Namn  | Kardinalitet      | Datatyp                         | Beskrivning|
| -- | :---------------:| :-------------------------------: | ---------------------- |
|[**source**](#source)|1|[heltal](#heltal)|**Obligatoriskt** - Ange organisationsnummret utan mellanslag eller bindesstreck för organisationen som hanterar parkeringen. |
|[**id**](#id)|1|heltal|**Obligatoriskt** - Anger en identifierare för parkeringen. |
|[**name**](#name)|1|text|**Obligatoriskt** - Anger namn på verksamheten där parkering skapats. |
|[**type**](#type)|1|public &vert; commercial &vert; residential|**Obligatoriskt** - Ange parkeringens typ.|
|[**latitude**](#latitude)|1|[decimal](#decimal)|**Obligatoriskt** - Latitude anges per format enligt WGS84.|
|[**longitude**](#longitude)|1|[decimal](#decimal)|**Obligatoriskt** - Longitude anges per format enligt WGS84.|
|[street](#street)|0..1|text|Ange gatuadress för parkeringen.|
|[postalcode](#postalcode)|0..1|[heltal](#heltal)|Ange postnummer för den parkeringen.|
|[city](#city)|0..1|text|Ange postort för den parkeringen.|
|[description](#description)|0..1|text|Anger en kortare beskrivning av parkeringen. |
|[car_spaces](#car_spaces)|0..1|heltal|Anger antalet parkeringsplatser för bilar. |
|[accessible_spots](#accessible_spots)|0..1|heltal|Anger antalet parkeringsplatser för funktionsvariationer. |
|[rv_spaces](#rv_spaces)|0..1|heltal|Anger antalet parkeringsplatser för husbilar. |
|[motorcycles](#motorcycles)|0..1|heltal|Anger antalet parkeringsplatser för motorcyklar. |
|[charging_points](#charging_points)|0..1|heltal|Anger antalet laddplatser. |
|[permit](#permit)|0..1|boolean|Anger om parkeringen kräver parkeringstillstånd. |
|[prohibition](#prohibition)|0..1|text|Anger om parkeringsförbud gäller, anges som t.ex. 02:00-06:00. |
|[parking_fee](#parking_fee)|0..1|boolean|Anger om parkeringen kräver avgift viss tid. |
|[public](#public)|0..1|boolean|Anger om parkeringen får användas offentligt. |
|[status](#status)|0..1|boolean|Anger om parkeringen är aktivt öppen. |
|[lighting](#lighting)|0..1|boolean|Anger om parkeringen är upplyst. |
|[disc](#disc)|0..1|boolean|Anger om parkeringen kräver p-skiva. |
|[disc_digital](#disc_digital)|0..1|boolean|Anger om parkeringen kräver digital p-skiva. |
|[updated](#updated)|0..1|text|Ange när parkeringen uppdaterades senast.|
|[email](#email)|0..1|text|E-postadress för vidare kontakt, anges med gemener och med @ som avdelare.|
|[URL](#url)|0..1|[URL](#url)|Ingångssida för mer information om parkeringen.|

</div>

## Förtydligande av datatyper

En del av datatyperna nedan förtydligas med hjälp av det som kallas reguljära uttryck. Dessa är uttryckta så att de matchar exakt, d.v.s. inga inledande eller eftersläpande tecken tillåts.

### **heltal**
Reguljärt uttryck: **`/^\-?\\d+$/`**

Heltal anges alltid som en radda siffror utan mellanrum eventuellt med ett inledande minus. Se [xsd:integer](https://www.w3.org/TR/xmlschema-2/#integer) för en längre definition. Utelämnat värde tolkas aldrig som noll (0) utan tolkas som “avsaknad av värde”.
 
### **decimal**
Reguljärt uttryck: **`/^\-?\\d+\\.\\d+$/`**

Decimaltal anges i enlighet med [xsd:decimal](https://www.w3.org/TR/xmlschema-2/#decimal). Notera att i Sverige används ofta decimalkomma inte punkt. För att vara enhetlig mellan olika dataformat ska decimalpunkt användas då den tabulära modellen använder komma som separator.

Den kanoniska representationen i xsd:decimal är påbjuden, d.v.s. inga inledande nollor eller +, samt att man alltid ska ha en siffra innan och efter decimalpunkt. Noll skrivs som 0.0 och ett utelämnat värde skall aldrig tolkas som noll (0) utan “avsaknad av värde”.

### **url**

En länk till en webbsida där parkeringen presenteras hos den lokala myndigheten eller arrangören.

Observera att man inte får utelämna schemat, d.v.s. "www.example.com" är inte en tillåten webbadress, däremot är "https://www.example.com" ok. Relativa webbadresser accepteras inte heller. (Ett fullständigt reguljärt uttryck utelämnas då den är både för omfattande och opedagogisk.)

Om du behöver ange flera URL:er måste du då sätta dubbelt citattecken och separera de olika URL:erna med kommatecken. Läs gärna med i RFC 4180 för CSV.

Kontrollera även innan du publicerar filen att det går att läsa in din fil utan problem. Det finns flera webbverktyg för att testa så kallad [parsing](https://sv.wikipedia.org/wiki/Parser) t.ex. [https://CSVLint.io](https://csvlint.io) som du kan använda för att testa din fil.

### **boolean**

Reguljärt uttryck: **`/^[true|false]?&/`**

I samtliga förekommande fall kan texten “true” eller “false” utelämnas.

Ett tomt fält skall tolkas som “okänt” eller “ej inventerat”. Ange enbart “true” eller “false” om du vet att egenskapen finns (true) eller saknas (false). Gissa aldrig.

Attributet är en så kallad Boolesk” datatyp och kan antingen ha ett av två värden: TRUE eller FALSE, men aldrig båda.

</div>

## Förtydligande av attribut

### **id**

Reguljärt uttryck: **`/^[a-zA-Z_:1-9]*$/`**

En unik identifierare för den parkeringen. 

### source

Ange organisationsnummret utan mellanslag eller bindesstreck för organisationen. Exempel för Gullspångs kommun: **2120001637**.

### name

Anger namn på verksamheten där parkering skapats. Exempelvis "Algården".

### description

Beskrivning av parkeringen.

### type

Anger typ av parkering, t.ex. boendeparkering.

### email

Funktionsadress till organisationen. Ange ej det inledande URI schemat mailto: eller HTML-koder i uttrycket. Exempel: info@organisation.se

### url 

En ingångssida. Anges med inledande schemat **https://** eller **http://** 
